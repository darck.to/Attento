(function() {
    //CARGA MAPA PRINCIPAL
    if (!map) {
        var map = L.map('portada-map', {tap: false}).setZoom(20);
    }
    //CAPA OPENSTREETMAP
    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: 'OpenStreet Maps'
    }).addTo(map);
    var location
    function onLocationFound(e) {
        location = e.latlng;
        L.marker(e.latlng).addTo(map);
        var geocodeService = L.esri.Geocoding.geocodeService();
        var marker
        map.on('click', function (e) {
            geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
                if (error) {
                    return;
                }
                if (marker) { //ELIMINA EL MARKER ANTERIOR
                    map.removeLayer(marker);
                }
                //POPUP
                var txt = '<div class="columns is-multiline is-mobile">';
                txt += '<div class="column has-text-centered">';
                    txt += '<p class="is-size-6 mt-0 mb-1">Crear reporte en: </p>'
                        txt += '<button class="button reporte-nuevo-datos">';
                            txt += '<span class="icon">';
                                txt += '<i class="fas fa-exclamation-circle"></i>';
                            txt += '</span>';
                            txt += '<span>' + result.address.Address + '</span>';
                        txt += '</div>';
                    txt += '</div>'
                marker = L.marker(result.latlng).addTo(map).bindPopup(txt).openPopup();
                //CREAR REPORTE A PARTIR DE UBICACION
                $('.reporte-nuevo-datos').on('click', function(re) {
                    $.get("templates/reporte/reporte-nuevo.html", function(htmlContent) {
                        modap(htmlContent);
                        $('.rep-0').val(result.address.Address);
                        $('.rep-1').val(result.address.AddNum);
                        $('.rep-2').val(result.address.Neighborhood);
                        $('.rep-3').val(result.address.City);
                        $('.rep-4').val(result.address.Subregion);
                        $('.rep-5').val(result.address.Region);
                        $('.rep-6').val(result.address.Postal);
                        $('.rep-7').val(result.address.CountryCode);
                        $('.rep-8').val(e.latlng.lat);
                        $('.rep-9').val(e.latlng.lng);
                    })
                })
            })
        })        
        //CREA EL BOTON DE HOME ZOOM
        $('#portada-map').prepend('<span class="map-center is-clickable is-size-2"><i class="far fa-compass"></i></span>');
        $('.map-center').on('click', function(e) {
            e.stopPropagation()
            map.setView(location, 20);
        })
    }
    function onLocationError(e) {
        toast('Erro: ' + e.message)
        
    }
    //LOCALIZACION
    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);
    map.locate({setView: true, maxZoom: 22})

    //CLICK EN EL MAPA
    map.on('click', function(e) {
        //toast('Click')
    });

    //CREAR REPORTE
    $('.reporte-nuevo').on('click', function(e) {
        $.get("templates/reporte/reporte-nuevo.html", function(htmlContent) {
            modap(htmlContent)
        })
    })
    //CARGAR PERFIL
    $('.perfil-carga').on('click', function(e) {
        $.get("templates/perfil/perfil.html", function(htmlContent) {
            modap(htmlContent)
        })
    })
}());