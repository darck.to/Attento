(function() {
    carga_main_login()
}());

function carga_main_login(e) {
    var stringarep_key = localStorage.getItem("rep_key");
    var stringarep_user = localStorage.getItem("rep_user");
    var loginToken = localStorage.getItem("rep_token");
    comprueba_login(stringarep_key,stringarep_user,loginToken);
    function comprueba_login(a,u,t) {
      //COMPROBAMOS SI EXISTE LOGIN LOCAL
      if (t == 0 || t == null) {
        //LOCAL LOGIN NULL OR NEGATIVE
        $.ajax({
          type: 'POST',
          url: 'php/init/init-comprueba-auth.php',
          data: {
            auth : a,
            user : u
          },
          async:true,
          dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
          success: function(data) {
            var aprobacion
            $.each(data, function (name, value) {
              if (value.success == true) {
                localStorage.setItem("rep_token",1);
                localStorage.setItem("rep_type",value.type);
                toast(value.message)
                //LOGIN LOCAL POSITIVE
                carga_principal()
              } else {
                modap();
                $('.modal-close').remove();
                localStorage.setItem("rep_token",0);
                $('.modal-content').load('templates/init/init-login.html')
              }
            });
          },
          error: function(xhr, tst, err) {
            toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
            modax();
          }
        })
      } else {
        //LOGIN LOCAL POSITIVE
        carga_principal()
      }
  
    }
  }

function carga_principal(e) {
    //CARGA MENU
    $('#menu').load('templates/menu/menu.html')
    
    //PEDIMOS LOCALIZACION
    var onSuccess = function(position) {
    localStorage.setItem("lat", position.coords.latitude);
    localStorage.setItem("lng", position.coords.longitude)
    //BACK BUTTON X2 TO EXIT credits to https://lloydzhou.github.io/project/2014/04/30/phonegap-exit-on-double-click-backbutton
    document.addEventListener('deviceready', function() {
        var exitApp = false, intval = setInterval(function (){exitApp = false;}, 1000);
        document.addEventListener("backbutton", function (e){
            e.preventDefault();
            if (exitApp) {
                clearInterval(intval)
                (navigator.app && navigator.app.exitApp()) || (device && device.exitApp())
            }
            else {
                exitApp = true;
                if ($('.modal').length) {
                    $('.modal').remove();
                }
                $(".navbar-burger").removeClass("is-active");
                $(".navbar-menu").removeClass("is-active")
            }
        }, false)
    }, false)
    //PORTADA PRINCIPAL
    $('#portada-container').load('templates/portada/portada-principal.html')
    }
    var onError = function(error) {
        toast('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n')
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError)
}