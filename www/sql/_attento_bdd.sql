-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-03-2022 a las 19:47:05
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `_attento_bdd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_table`
--

CREATE TABLE `auth_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `init_index` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'init index',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'usuario de login',
  `pas` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'password de usuario',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `auth_table`
--

INSERT INTO `auth_table` (`id`, `init_index`, `nom`, `pas`, `id_usr`) VALUES
(5, 'tSCnwkhD', 'Beto', '$2y$10$sFwV6hDmb5DqaoZgy6q5JOSZK7edeitFIhpqVqcqrWz1qgfXZm8pS', 'g2Xk6Ku7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_table`
--

CREATE TABLE `direccion_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `id_reporte` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del reporte',
  `cal` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'calle del reporte',
  `num` int(11) NOT NULL COMMENT 'numero del reporte ',
  `col` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'colonia del reporte',
  `ciu` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'ciudad del reporte',
  `mun` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'municipio del reporte',
  `est` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'estado del reporte',
  `cp` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'codigo postal del reporte',
  `cd` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'ciudad del reporte',
  `latd` int(11) NOT NULL COMMENT 'latitudo del device',
  `lngd` int(11) NOT NULL COMMENT 'longitud del device'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perf_table`
--

CREATE TABLE `perf_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `type` int(11) NOT NULL COMMENT 'Tipo de user',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apm` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `mai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'mail del usr',
  `fec` datetime NOT NULL COMMENT 'fecha de creacion de perfil',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `perf_table`
--

INSERT INTO `perf_table` (`id`, `id_usr`, `type`, `nom`, `ape`, `apm`, `tel`, `cel`, `mai`, `fec`, `id_per`) VALUES
(5, 'g2Xk6Ku7', 1, 'Beto', '', '', '', '', 'm@il.com', '0000-00-00 00:00:00', 'SbbLn1JN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporte_table`
--

CREATE TABLE `reporte_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_reporte` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del reporte',
  `id_perfil` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `id_tipo` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del tipo de reporte',
  `fla` bit(1) NOT NULL COMMENT 'estatus del reporte',
  `lat` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'latitud del reporte',
  `lng` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'longitud del reporte'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direccion_table`
--
ALTER TABLE `direccion_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reporte_table`
--
ALTER TABLE `reporte_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `direccion_table`
--
ALTER TABLE `direccion_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla';

--
-- AUTO_INCREMENT de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `reporte_table`
--
ALTER TABLE `reporte_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
