<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  include_once('../functions/abre_conexion.php');
  
  $key = mysqli_real_escape_string($mysqli,$_POST['key']);
  $usr = mysqli_real_escape_string($mysqli,$_POST['usr']);
  $data = mysqli_real_escape_string($mysqli,$_POST['data']);
  //FUNCTIONS AUTH
  include_once('../functions/_functions_auth.php');
 
  $resultados[] = array("success"=> authCheck($key,$usr));

  include_once('../functions/cierra_conexion.php');

	print json_encode($resultados);
?>
