<?php
	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZasdfgqwertzxcvbpoiuylkjhmn';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	//FUNCTION COMPRUEBA EMAIL
  	function validaEmail($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}
	//PERFIL ESTRUCTURA//////////////////////////////
	function perfiles_crea_estrutura($usuario) {
	    //CARPETA DE TIENDA Y PRODUCTOS
		$dir = '../../data/usr/' . $usuario . '/';
		$return = false;
		if (mkdir($dir, 0755, true)) {
		  $return = true;		  
		}
		return $return;
	}
?>
