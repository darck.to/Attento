(function() {
    //CARGA MENU
    $('#menu').load('templates/menu/menu.html')

    //PEDIMOS LOCALIZACION
    var onSuccess = function(position) {
        localStorage.setItem("lat", position.coords.latitude);
        localStorage.setItem("lng", position.coords.longitude)
        //BACK BUTTON X2 TO EXIT credits to https://lloydzhou.github.io/project/2014/04/30/phonegap-exit-on-double-click-backbutton
        document.addEventListener('deviceready', function() {
            var exitApp = false, intval = setInterval(function (){exitApp = false;}, 1000);
            document.addEventListener("backbutton", function (e){
                e.preventDefault();
                if (exitApp) {
                    clearInterval(intval)
                    (navigator.app && navigator.app.exitApp()) || (device && device.exitApp())
                }
                else {
                    exitApp = true;
                    if ($('.modal').length) {
                        $('.modal').remove();
                    }
                    $(".navbar-burger").removeClass("is-active");
                    $(".navbar-menu").removeClass("is-active")
                }
            }, false)
        }, false)
        //PORTADA PRINCIPAL
        $('#portada-container').load('templates/portada/portada-principal.html')
    }
    var onError = function(error) {
        toast('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n')
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError)
}());