(function() {
    //CARGA MAPA PRINCIPAL
    if (!map) {
        var map = L.map('portada-map', {tap: false}).setZoom(19);
    }
    //CAPA OPENSTREETMAP
    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: 'OpenStreet Maps'
    }).addTo(map)
    function onLocationFound(e) {
        L.marker(e.latlng).addTo(map);
        var geocodeService = L.esri.Geocoding.geocodeService();
        var marker
        map.on('click', function (e) {
            geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
                if (error) {
                    return;
                }
                if (marker) { //ELIMINA EL MARKER ANTERIOR
                    map.removeLayer(marker);
                }
                //POPUP
                var txt = '<div class="columns is-multiline is-mobile">';
                    txt += '<div class="column is-8"><b>Crear reporte en: </b>' + result.address.Match_addr + '</div>'
                    txt += '<div class="column is-4 is-size-3 has-text-centered is-clickable reporte-nuevo-datos"><i class="fas fa-exclamation"></i></div>'
                txt += '</div>'
                marker = L.marker(result.latlng).addTo(map).bindPopup(txt).openPopup();
                //CREAR REPORTE A PARTIR DE UBICACION
                $('.reporte-nuevo-datos').on('click', function(re) {
                    $.get("templates/reporte/reporte-nuevo.html", function(htmlContent) {
                        modal(htmlContent);
                        $('.rep-0').val(result.address.Address);
                        $('.rep-1').val(result.address.AddNum);
                        $('.rep-2').val(result.address.Neighborhood);
                        $('.rep-3').val(result.address.City);
                        $('.rep-4').val(result.address.Subregion);
                        $('.rep-5').val(result.address.Region);
                        $('.rep-6').val(result.address.Postal);
                        $('.rep-7').val(result.address.CountryCode);
                        $('.rep-8').val(e.latlng.lat);
                        $('.rep-9').val(e.latlng.lng);
                    })
                })
            })
        })
    }
    function onLocationError(e) {
        toast('Erro: ' + e.message)
        
    }
    //LOCALIZACION
    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);
    map.locate({setView: true, maxZoom: 22})

    //CLICK EN EL MAPA
    map.on('click', function(e) {
        //toast('Click')
    });


    //CREAR REPORTE
    $('.reporte-nuevo').on('click', function(e) {
        $.get("templates/reporte/reporte-nuevo.html", function(htmlContent) {
            modal(htmlContent)
        })
    })
}());