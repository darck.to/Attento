(function() {
    // Take photo from camera
    $('.rep-cam').on('click', function(){
        inum = $(this).attr('val');
        navigator.camera.getPicture(onSuccess, onFail, { quality: 90,
          correctOrientation: true,
          targetWidth: 900,
          targetHeight: 900,
          destinationType: Camera.DestinationType.FILE_URL
        })
    })

    // Select from gallery
    $(".rep-gal").on('click', function(){
        inum = $(this).attr('val');
        navigator.camera.getPicture(onSuccess, onFail, { quality: 90,
            targetWidth: 900,
            targetHeight: 900,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            destinationType: Camera.DestinationType.FILE_URI
        })
    })

    // Change image source
    function onSuccess(imageData) {
        var url = "php/products/products-preimg.php";
        var image = document.getElementById('imgContainer' + inum);
        image.src = imageData + '?' + Math.random();

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";

        var params = {};
        params.auth = localStorage.getItem("aUth_key");
        params.user = localStorage.getItem("aUth_user");
        //SI ES EDICION
        if (localStorage.getItem("edit_id") != null) {
            params.id = localStorage.getItem("edit_id");
            url = "php/products/products-editimg.php";
        }
        params.num = inum;

        options.params = params;
        options.chunkedMode = false;

        var ft = new FileTransfer();
        ft.upload(imageData, url, function(result){
            $.each(result,function(index,content){
            if (content.success == true) {
                toast(content.img)
            }
            })
            cierra_loader();
        }, function(error){
            cierra_loader();
            alert('error : ' + JSON.stringify(error));
        }, options);
    }

    function onFail(message) {
        toast('No seleccionaste imagen, error: ' + message);
    }

    //ACCIONES INPUT SUBMIT
    $('.clear-reporte').on('click', function(e) {
        e.preventDefault();
        modax()
    })
    $('.save-reporte').on('click', function(e) {
        e.preventDefault()
        var inputValues = {}
        $('.registro-input').each(function() {
            inputValues[$(this).attr('placeholder')] = $(this).val();
        })
        inputValues = JSON.stringify(inputValues)
        //ENVIAMOS VIA PHP
        var formData = new FormData();
        formData.append('DATA', inputValues)
        $.ajax({
            type: 'POST',
            url: 'php/reporte/reporte-nueva.php',
            data: formData,
            processData: false,
            contentType: false,
            async: true,
            dataType: 'json',
            crossDomain: true,
            context: document.body,
            cache: false
        })
        .done(function(data) {
            $.each(data, function (name, value) {
                if (value.success) {
                    toast(value.message)
                } else {
                    toast(value.message)
                }
            })
        })
        .fail(function() {
            toast("aix error");
        })
    })
}());